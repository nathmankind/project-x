from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.http import Http404
import random
from django.contrib.auth.models import User, Group
from .models import GoalStatus, ScrumyGoals, ScrumyHistory
from .forms import SignupForm, CreateGoalForm, MoveGoalForm, DevCreateGoalForm, DevMoveGoalForm, QAMoveGoalForm

# Create your views here.


def index(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            group = form.cleaned_data['group_choices']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()
            user_group = Group.objects.get(id=int(group))
            user_group.user_set.add(user)
            return render(request, 'nathmankindscrumy/success.html', {})
    else:
        form = SignupForm()

    return render(request, 'nathmankindscrumy/index.html', {'form': form})


def move_goal(request, goal_id):
    if request.user.is_authenticated:
        current_user = request.user
        user_group = request.user.groups.all()[0].name
        dev_grp = Group.objects.get(name = 'Developer')
        admin_grp = Group.objects.get(name = 'Admin')
        qa_grp = Group.objects.get(name = 'Quality Assurance')
        owner_grp = Group.objects.get(name = 'Owner')
        dev = dev_grp.name
        admin = admin_grp.name
        qa = qa_grp.name
        owner = owner_grp.name
        try:
            goal = ScrumyGoals.objects.get(goal_id = goal_id)
            if user_group == admin or user_group == owner:
                if request.method == 'POST':
                    form = MoveGoalForm(request.POST)
                    if form.is_valid():
                        selected_status = form.save(commit=False)
                        status = form.cleaned_data.get('goal_status')
                        choice = GoalStatus.objects.get(id = int(status))
                        goal.goal_status = choice
                        goal.save()
                        return render(request, 'nathmankindscrumy/move_success.html', {'goal': goal})
                else:
                    form = MoveGoalForm()
            elif user_group == dev and request.user == goal.user:
                if request.method == 'POST':
                    form = DevMoveGoalForm(request.POST)
                    if form.is_valid():
                        selected_status = form.save(commit=False)
                        status = form.cleaned_data.get('goal_status')
                        choice = GoalStatus.objects.get(id = int(status))
                        goal.goal_status = choice
                        goal.save()
                        return render(request, 'nathmankindscrumy/move_success.html', {'goal': goal})
                else:
                    form = DevMoveGoalForm()
            elif user_group == qa:
                if request.method == 'POST':
                    form = QAMoveGoalForm(request.POST)
                    if form.is_valid():
                        selected_status = form.save(commit=False)
                        status = form.cleaned_data.get('goal_status')
                        choice = GoalStatus.objects.get(id = int(status))
                        goal.goal_status = choice
                        goal.save()
                        return render(request, 'nathmankindscrumy/move_success.html', {'goal': goal})
                else:
                    form = QAMoveGoalForm()
            else:
                return render(request, 'nathmankindscrumy/error.html', {})
            return render(request, 'nathmankindscrumy/movegoal.html', {'form': form})
        except ScrumyGoals.DoesNotExist:
            return render(request, 'nathmankindscrumy/exception.html', {'error': 'A record with that goal id does not exist'})
    
    else:
        return HttpResponse('Please login')
    # try:
    #     obj = ScrumyGoals.objects.get(goal_id=goal_id)
    # except ScrumyGoals.DoesNotExist:
    #     return render(request, 'nathmankindscrumy/exception.html', {'error': 'A record with that goal id does not exist'})
    


def add_goal(request):
    current_user = request.user
    user_group = request.user.groups.all()[0].name
    dev_grp = Group.objects.get(name = 'Developer')
    admin_grp = Group.objects.get(name = 'Admin')
    qa_grp = Group.objects.get(name = 'Quality Assurance')
    owner_grp = Group.objects.get(name = 'Owner')
    dev = dev_grp.name
    admin = admin_grp.name
    qa = qa_grp.name
    owner = owner_grp.name
    if user_group == dev or user_group == qa or user_group == owner:
        if request.method == 'POST':
            form = DevCreateGoalForm(request.POST)
            if form.is_valid():
                goal = form.save(commit=False)
                user = current_user
                goal_stat = form.cleaned_data.get('goal_status')
                status = GoalStatus.objects.get(id = int(goal_stat))
                goal_name = form.cleaned_data['goal_name']
                goal.goal_id = random.randint(1000, 9999)
                goal.goal_status = status
                goal.user = user
                goal.save()
                return render(request, 'nathmankindscrumy/goal_success.html', {})
        else:
            form = DevCreateGoalForm()
        
        
            #==========
    elif user_group == admin:
        if request.method == 'POST':
            form = CreateGoalForm(request.POST)
            if form.is_valid():
                goal = form.save(commit=False)
                user = form.cleaned_data.get('user')
                goal_stat = form.cleaned_data.get('goal_status')
                goal_name = form.cleaned_data['goal_name']
                goal.goal_id = random.randint(1000, 9999)
                goal.goal_status = goal_stat
                goal.user = user
                goal.save()
                return render(request, 'nathmankindscrumy/goal_success.html', {})
        else:
            form = CreateGoalForm()

    return render(request, 'nathmankindscrumy/addgoal.html', {
            'form': form,
            'current_user': current_user,
            'user_grp': user_group
        })


def home(request):
    current_user = request.user
    user_group = request.user.groups.all()[0].name
    if request.user.is_authenticated:
        users = User.objects.all()
        '''
        1 = Weekly Goal,
        2 = Daily Goal,
        3 = verify goal,
        4 = done goal
        '''

        all_goal = []
        for goal in users:
            user_goals = goal.user.all()
            all_goal.append((user_goals))
        context = {
            'users': users,
            'current_user': current_user,
            'all_goals': all_goal,
            'user_group': user_group
        }
        return render(request, 'nathmankindscrumy/home.html', context)
    else:
        return render(request, 'registration/login.html', {'error': 'Please login'})

