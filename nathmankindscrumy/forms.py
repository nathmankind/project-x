from nathmankindscrumy.models import ScrumyGoals, ScrumyHistory, GoalStatus
from django.contrib.auth.models import User, Group
from django import forms


class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    group_choices = forms.ChoiceField(choices=[(choice.pk, choice) for choice in Group.objects.all()],
                                      widget=forms.RadioSelect)
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password', 'group_choices']


class CreateGoalForm(forms.ModelForm):
    '''user = forms.ChoiceField(choices=[(choice.pk, choice) for choice in User.objects.all()],
    widget=forms.RadioSelect)
    user = forms.ModelMultipleChoiceField(queryset=User.objects.all())'''
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name', ]

    def __init__(self, *args, **kwargs):
        super(CreateGoalForm, self).__init__(*args, **kwargs)
        self.fields['user'] = forms.ModelChoiceField(
            queryset=User.objects.all())
        self.fields['goal_status'] = forms.ModelChoiceField(
            queryset=GoalStatus.objects.all())

class DevCreateGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name']
    
    def __init__(self, *args, **kwargs):
        super(DevCreateGoalForm, self).__init__(*args, **kwargs)
        queryset=GoalStatus.objects.all()
        self.fields['goal_status'] = forms.ChoiceField(
            choices=[(choice.pk, choice) for choice in queryset[:1]])
    
# class MoveGoalForm(forms.ModelForm):
#     goal_status = forms.ModelChoiceField(queryset=GoalStatus.objects.all())

#     class Meta:
#         model = GoalStatus
#         fields = ['goal_status']
# ChangeGoalForm
class MoveGoalForm(forms.ModelForm):
    queryset = GoalStatus.objects.all()
    goal_status = forms.ChoiceField(
        choices=[(choice.pk, choice) for choice in queryset])

    class Meta:
        model = GoalStatus
        fields = ['goal_status']
    
class DevMoveGoalForm(forms.ModelForm):
    queryset = GoalStatus.objects.all()
    goal_status = forms.ChoiceField(
        choices=[(choice.pk, choice) for choice in queryset[:3]])

    class Meta:
        model = GoalStatus
        fields = ['goal_status']

class QAMoveGoalForm(forms.ModelForm):
    queryset = GoalStatus.objects.all()
    goal_status = forms.ChoiceField(
        choices=[(choice.pk, choice) for choice in queryset.order_by('-id')[:3][::-1]])

    class Meta:
        model = GoalStatus
        fields = ['goal_status']