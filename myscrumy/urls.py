
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('nathmankindscrumy/', include('nathmankindscrumy.urls')),
    path('websocket/', include('websocket.urls')),
]
