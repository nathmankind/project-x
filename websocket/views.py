from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Connection, ChatMessage
import json #from websocket.models import Connection
import boto3
from django.forms.models import model_to_dict


# Create your views here.

def test(request):
    return JsonResponse({'message': 'hello Daud'}, status=200)


def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)


@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    Connection.objects.create(connection_id = connection_id)
    response = {
        "statusCode": 200,
        "body": "Connecte succesfully"
    }
    return JsonResponse('connect successfully', status=200, safe=False)

@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connect_id = Connection.objects.get(connection_id = connection_id)
    connect_id.delete()
    return JsonResponse('disconnect successfully', status=200, safe=False)

@csrf_exempt
def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client('apigatewaymanagementapi', 
                                endpoint_url='https://0xt7o23jpd.execute-api.us-west-2.amazonaws.com/test/', 
                                region_name = 'us-west-2', aws_access_key_id = 'AKIAI2LVZLVQPF6VWBGQ',
                                aws_secret_access_key = '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')
    return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))

@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    chat_message = ChatMessage.objects.create(
        username=body['body']["username"], message=body['body']["message"], timestamp=body['body']["timestamp"])
    connections = [i.connection_id for i in Connection.objects.all()]
    body = {'username': chat_message.username,
            'message': chat_message.message, 'timestamp': chat_message.timestamp}
    data = {'messages': [body]}
    for connection in connections:
        _send_to_connection(connection, data)
    return JsonResponse('successfully sent', status=200, safe=False)
# def send_message(request):
#     body = _parse_body(request.body)
#     chat_message  = ChatMessage.objects.create(username = body["body"]["username"], message = body["body"]["message"], timestamp = body["body"]["timestamp"])
#     connections = [i.connection_id for i in Connection.objects.all()]
#     body = {
#         'username':chat_message.username,
#         'message':chat_message.message,
#         'timestamp':chat_message.timestamp
#     }
#     data = {'messages':[body]}
#     print(data)
#     for connection in connections:
#         _send_to_connection(connection, data)
#     return JsonResponse('successfuly sent', status=200, safe=False)
    

@csrf_exempt
def get_recent_messages(request):
    body = _parse_body(request.body)
    connectionId = body['connectionId']
    connection_id = Connection.objects.get(
        connection_id=connectionId).connection_id
    messages = list(reversed(ChatMessage.objects.all()))
    if len(messages) > 5:
        data = {'messages': [{'username': chat_message.username, 'message': chat_message.message,
                              'timestamp': chat_message.timestamp} for chat_message in messages[:5]]}
    else:
        data = {'messages': [{'username': chat_message.username, 'message': chat_message.message,
                              'timestamp': chat_message.timestamp} for chat_message in messages]}
    _send_to_connection(connection_id, data)
    return JsonResponse('successfully sent', status=200, safe=False)


# def get_recent_message(request):
#     body = _parse_body(request.body)
#     connectionId = body['connectionId']
#     connection_id = Connection.objects.get(connection_id = connectionId).connect_id
#     messages = list(reversed(ChatMessage.objects.all()))
#     if len(messages) > 5:
#         data = {'messages': [{
#             'username':chat_message.username, 
#             'message': chat_message.message, 
#             'timestamp':chat_message.timestamp} for chat_message in messages[:5]]}
#     else:
#         data = {'messages':[{
#             'username':chat_message.username, 
#             'message': chat_message.message, 
#             'timestamp':chat_message.timestamp} for chat_message in messages]}
#     _send_to_connection(connection_id, data)
#     return JsonResponse('received successfully', status=200, safe=False)


# @csrf_exempt
# def get_recentmessages(request):
#     body = _parse_body(request.body)
#     connection_id = body['connectionId']
# ​
#     for chat in ChatMessage.objects.all():
#         the_data = {"message":[{"username":chat.username, "message":chat.message, "timestamp":chat.timestamp}]}
#         data = json.dumps(the_data)
#         print(data)
#         _send_to_connection(connection_id, data)